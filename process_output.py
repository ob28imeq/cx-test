#!/usr/bin/env python

import argparse
import logging
import os

import cbutil as cb

import processors
from pathlib import Path

logger = logging.getLogger(__file__)


def get_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--type",
        help=(
            "Determine how file is processed is ",
            "(mesa_pd|pfac_csv|mesa_pd_sqlite|uniformgrid_sqlite|energy_json|gravitywave_sqlite|percolation_sqlite)",
        ),
        required=True,
    )
    parser.add_argument(
        "-ho", "--host", help="The host the machine was executed", required=True
    )
    parser.add_argument(
        "-c",
        "--cores",
        help="Number of cores used, default=1",
        required=False,
        default=1,
    )
    parser.add_argument(
        "-d",
        "--dryrun",
        help="proccess but do not upload data, default=false",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-log",
        "--loglevel",
        default="warning",
        help=("Provide logging level. " "Example --loglevel debug, default=warning"),
    )

    parser.add_argument(
        "-pg",
        "--performance_group",
        type=str,
        default="MEM_DP",
        required=False,
        help=("Path to a json file from likwid."),
    )

    parser.add_argument("output_file")
    return parser


def get_repo_infos_from_env():
    """Loads the values defined in global_variables.yml."""
    instance_url = os.getenv("WALBERLA_GITLAB_INSTANCE",
                             "https://i10git.cs.fau.de")
    project_id = os.getenv("WALBERLA_PROJECT_ID", "walberla/walberla")
    branch = os.getenv("WALBERLA_BRANCH", "master")
    return instance_url, project_id, branch


def set_loglevel(loglevel: str):
    logging.basicConfig(level=loglevel.upper())


def main():
    parser = get_argparser()
    args = parser.parse_args()
    set_loglevel(args.loglevel)

    logger.info(args)
    up = cb.Uploader()

    if not os.path.exists(args.output_file):
        raise ValueError(f"{args.output_file} does not exists!")

    common_tags = dict()

    if args.type == "mesa_pd":
        common_tags.update({"num_cores": args.cores})
        data_points = processors.mesa_pd(args.output_file)
    elif hasattr(processors, args.type):
        data_points = getattr(processors, args.type)(
            args.output_file, perf_group=args.performance_group
        )
    else:
        raise ValueError("Unsupported type")

    measurements = [*data_points]

    instance_url, project_id, branch = get_repo_infos_from_env()
    common_tags.update(
        cb.get_git_infos_from_api(
            instance_url, project_id, branch=branch, commit_env_key="WALBERLA_COMMIT"
        )
    )
    common_tags.update({"host": args.host})
    common_tags.update({"branch": branch})
    common_tags.update({"project_id": project_id})
    # also adde the commit of the proxy repository
    common_tags.update(
        cb.util.get_git_infos(
            ".", commit_key="proxy_commit", commit_msg_key="proxy_commit_msg"
        )
    )
    if args.output_file.startswith("gpu"):
        *_, gpu_name = Path(args.output_file).stem.split("_")
        common_tags.update({"GPU": gpu_name})

    up.upload(measurements, dry_run=args.dryrun,
              tags=common_tags, time_precision="s")


if __name__ == "__main__":
    main()
