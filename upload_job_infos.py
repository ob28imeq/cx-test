import logging

import cbutil as cb

logging.basicConfig(level=logging.INFO)


def main():
    up = cb.Uploader()
    url = cb.get_url_from_env()
    up.upload([dp.asdict() for dp in cb.get_job_datapoints(url)])


if __name__ == "__main__":
    main()
