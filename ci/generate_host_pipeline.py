import cbutil as cb
import os
import argparse
import re
from collections import ChainMap
from cbutil.yaml_util import print_yaml_file, load_script

BENCHMARK_FILE = "benchmarks.json"
HOST_INFO_FILE = "host_infos.json"

WALBERLA_GITLAB_INSTANCE = os.getenv("WALBERLA_GITLAB_INSTANCE", "")
WALBERLA_PROJECT_ID = os.getenv("WALBERLA_PROJECT_ID", "")
WALBERLA_BRANCH = os.getenv("WALBERLA_BRANCH", "")
WALBERLA_COMMIT = os.getenv("WALBERLA_COMMIT", "")
ON = "ON"
OFF = "OFF"


def print_file_header():
    config = {
        'stages': [
            'build',
            'benchmark',
            'final'
        ],
        'include': ["ci/base_jobs.yml"],
        'variables': {
            'WALBERLA_GITLAB_INSTANCE': WALBERLA_GITLAB_INSTANCE,
            'WALBERLA_PROJECT_ID': WALBERLA_PROJECT_ID,
            'WALBERLA_BRANCH': WALBERLA_BRANCH,
            'WALBERLA_COMMIT': WALBERLA_COMMIT
        }
    }

    config['.export_variables'] = {
        'before_script': [
            f'export WALBERLA_GITLAB_INSTANCE="{WALBERLA_GITLAB_INSTANCE}"',
            f'export WALBERLA_PROJECT_ID="{WALBERLA_PROJECT_ID}"',
            f'export WALBERLA_BRANCH="{WALBERLA_BRANCH}"',
            f'export WALBERLA_COMMIT="{WALBERLA_COMMIT}"'
        ]
    }
    return config


def print_gpu_compiler(gpu_type):
    if gpu_type == "amd":
        return {'USE_CUDA': OFF, 'USE_HIP': ON}
    if gpu_type == "nvidia":
        return {'USE_CUDA': ON, 'USE_HIP': OFF}
    return {'USE_CUDA': OFF, 'USE_HIP': OFF}


def generate_build_job(host, gpu_type, benchmarks_to_build, *, extra_modules=""):
    return {
        f'build_{host}': {
            'stage': 'build',
            'extends': ['.submit_job', '.seralize'],
            'variables': {
                'HOST': f'{host}',
                'SCRIPT': 'jobscript/build.sh',
                'USE_MPI': 'ON',
                'USE_OMP': 'OFF',
                **print_gpu_compiler(gpu_type),
                'BENCHMARKS_TO_BUILD': f"{benchmarks_to_build}",
                'HOST_SPECIFIC_MODULES': f"{extra_modules}"
            },
            'before_script': [
                *load_script("!reference [.export_variables, before_script]")
            ],
            'allow_failure': False
        }
    }


def print_benchmark_job(job_name,
                        host,
                        job_script,
                        processing_type,
                        datafile,
                        gpu_type,
                        visable_device,
                        after_script,
                        *,
                        extra_modules=""
                        ):
    job = {
        job_name: {
            'stage': 'benchmark',
            'extends': ['.submit_job', '.seralize'],
            'when': 'always',
            'variables': {
                'HOST': f'{host}',
                'SCRIPT': f'{job_script}',
                'TYPE': f'{processing_type}',
                'DATAFILE': f'{datafile}',
                'USE_MPI': 'ON',
                'USE_OMP': 'OFF',
                **print_gpu_compiler(gpu_type),
                'PERF_GROUP': 'MEM_DP',
                'LOG_LEVEL': 'info',
                'VISABLE_DEVICE': str(visable_device),
                'HOST_SPECIFIC_MODULES': f"{extra_modules}"
            },
            'needs': [
                {
                    'job': f'build_{host}',
                    'artifacts': False
                }
            ],
            'before_script': [
                *load_script('!reference [.export_variables, before_script]')
            ]
        }
    }
    if after_script:
        job[job_name]['after_script'] = [*after_script]

    return job


def generate_benchmark_job(host_infos: dict, benchmark_infos: dict):
    host = host_infos["name"]
    gpu_type = host_infos.get("gpu_type", "")
    num_gpus = host_infos.get("num_gpus", 0)
    extra_modules = host_infos.get("modules", "")

    benchmark = benchmark_infos["name"]
    job_script = benchmark_infos["jobscript"]
    processing_type = benchmark_infos["processingtype"]
    datafile = benchmark_infos["datafile"]
    after_script = [*load_script(benchmark_infos["after_script"])]

    job_name = f"benchmark_{host}_{benchmark}"
    if benchmark_infos["type"].lower() == "cpu":
        return [print_benchmark_job(
            f"{job_name}",
            host,
            job_script,
            processing_type,
            datafile,
            gpu_type,
            0,
            after_script,
            extra_modules=extra_modules)]

    return [
        print_benchmark_job(
            f"{job_name}:gpu{vis_device}",
            host,
            job_script,
            processing_type,
            datafile,
            gpu_type,
            vis_device,
            after_script,
            extra_modules=extra_modules)
        for vis_device in range(num_gpus)
    ]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("host")
    args = parser.parse_args()
    benchmarks = cb.json2dict(BENCHMARK_FILE)["benchmarks"]
    host_infos = cb.json2dict(HOST_INFO_FILE)["testcluster"].get(
        args.host, {"name": args.host, "gpu_type": "", "num_gpus": 0})

    benchmarks_to_build = " ".join([b["buildpath"]
                                   for b in benchmarks if not b.get("skip", False)])
    yaml_file = print_file_header()
    yaml_file.update(generate_build_job(host_infos["name"],
                                        host_infos["gpu_type"],
                                        benchmarks_to_build,
                                        extra_modules=host_infos.get(
                                            "modules", "")))

    for benchmark in benchmarks:
        if benchmark.get("skip", False):
            continue
        if not re.match(benchmark["project_id_regex"], WALBERLA_PROJECT_ID):
            continue
        if not re.match(benchmark["branch_regex"], WALBERLA_BRANCH):
            continue
        yaml_file.update(
            ChainMap(*generate_benchmark_job(host_infos, benchmark)))
    print(print_yaml_file(yaml_file))


if __name__ == "__main__":
    main()
