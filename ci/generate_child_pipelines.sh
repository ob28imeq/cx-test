#!/bin/bash

NOT_TO_USE='broadep2 aurora1 warmup phinally interlagos1 ivyep1 rome2 applem1studio lukewarm milan1 gracehop1 gracesup1'

if [ -z "${HOSTLIST}" ];
then
    HOSTLIST=$(sinfo -t idle -h --partition=work -o "%n %t" | grep -v "maint" | cut -d ' ' -f 1)
fi

cat << EOF
include:
  - "ci/lock.yml"
  - "ci/common.yml"

stages:
  - generate_host_pipeline
  - execute_host_pipeline
EOF

for HOST in $HOSTLIST; do
    if [[ $NOT_TO_USE == *"$HOST"* ]]; then
        continue
    fi

    cat << EOF

generate_${HOST}_pipeline:
  stage: generate_host_pipeline
  extends: .host_serializer
  tags:
    - testcluster
  when: always
  variables:
    NO_SLURM_SUBMIT: 1
  script:
    - export BENCHMARKS="${BENCHMARKS}"
    - export GPU_BENCHMARKS="${GPU_BENCHMARKS}"
    - export WALBERLA_GITLAB_INSTANCE="${WALBERLA_GITLAB_INSTANCE}"
    - export WALBERLA_PROJECT_ID="${WALBERLA_PROJECT_ID}"
    - export WALBERLA_BRANCH="${WALBERLA_BRANCH}"
    - export WALBERLA_COMMIT="${WALBERLA_COMMIT}"
    - !reference [.load_cbutil, before_script]
    - python3 ./ci/generate_host_pipeline.py "${HOST}" > ${HOST}-pipeline.yml
    - !reference [.load_cbutil, after_script]
  artifacts:
    paths:
      - ${HOST}-pipeline.yml
EOF
done

for HOST in $HOSTLIST; do
    if [[ $NOT_TO_USE == *"$HOST"* ]]; then
        continue
    fi
    cat<<EOF

execute_${HOST}_pipeline:
  stage: execute_host_pipeline
  extends: .host_serializer
  trigger:
    include:
      - artifact: ${HOST}-pipeline.yml
        job: generate_${HOST}_pipeline
    forward:
      pipeline_variables: true
    strategy: depend
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID
  needs:
    - generate_${HOST}_pipeline
EOF
done
