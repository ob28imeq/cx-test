#!/bin/bash -l

cat << 'EOF'
#!/bin/bash -l
#
#SBATCH --nodes=1
#SBATCH --constraint=hwperf
#SBATCH --time=01:00:00
#SBATCH --export=None
#SBATCH --exclusive
#SBATCH --output=%x.o%j.log
EOF

#Interpreted string
cat << EOF
# BUILD VARIABLES
USE_MPI="${USE_MPI}"
USE_CUDA="${USE_CUDA}"
USE_HIP="${USE_HIP}"
USE_OMP="${USE_OMP}"
VISABLE_DEVICE="${VISABLE_DEVICE:-"0"}"
CUDA_COMPUTE_CAP="OFF"

BENCHMARKS_TO_BUILD="${BENCHMARKS_TO_BUILD}"

PERF_GROUP="${PERF_GROUP:-"MEM_DP"}"
# Repository
WALBERLA_GITLAB_INSTANCE="${WALBERLA_GITLAB_INSTANCE}"
WALBERLA_PROJECT_ID="${WALBERLA_PROJECT_ID}"
WALBERLA_BRANCH="${WALBERLA_BRANCH}"
WALBERLA_COMMIT="${WALBERLA_COMMIT}"
EOF
cat << 'EOF'
WALBERLA_URL="${WALBERLA_GITLAB_INSTANCE}/${WALBERLA_PROJECT_ID}"
EOF

if [ -n "${HOST_SPECIFIC_MODULES}" ]; then
    for module in "$HOST_SPECIFIC_MODULES";
    do
        echo "module load ${module}"
    done
fi
cat << 'EOF'

MODULEPATH=$MODULEPATH:/opt/modules/modulefiles/testcluster

module load likwid/5.4.0
module load cmake
module load python/3.10-anaconda

if [ "${USE_CUDA}" = "ON" ];
then
    module load cuda
    CUDA_COMPUTE_CAP=$(nvidia-smi --query-gpu=compute_cap --format=csv,noheader | uniq | tr -d '.' | tr '\n' ';' | sed 's/;$//')
fi
[ "${USE_MPI}" = "ON" ] && module load openmpi/4.1.1-gcc9.3-legacy

module list

# Set ENVVARIABLES for testcluster
export LD_LIBRARY_PATH=$LIBRARY_PATH:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/slurm/current-u2004/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/slurm/23.02.3/lib/
export LC_CTYPE=C

# Directory Setup
BASE_DIR="/scratch"
[ -d "${BASE_DIR}" ] || BASE_DIR="${WORK}"
SOURCE_DIR="walberla_ci"
FULL_PATH="${BASE_DIR}/${SOURCE_DIR}"
BUILD_DIR="${FULL_PATH}/build"
VENV_NAME="walberla_venv"

if { conda env list | grep "$VENV_NAME"; } >/dev/null 2>&1; then
    echo "Found $VENV_NAME"
else
    conda create -n "$VENV_NAME" python=3.10 -y
fi
conda activate "$VENV_NAME"

pip install --upgrade pip setuptools wheel jinja2 py-cpuinfo lbmpy pystencils
pip list


LIKWIDPRFCTR=""
PERFCTR_AVAIL="$(likwid-perfctr -a | grep ${PERF_GROUP})"
if [ -n "${PERFCTR_AVAIL}" ];
then
    LIKWIDPRFCTR="likwid-perfctr -g $PERF_GROUP"
fi

SET_FREQ=1
command -v likwid-setFrequencies || SET_FREQ=0

BASE_CLOCK="2000.0"
#-- fix frequency
if [ "$SET_FREQ" -eq 1 ];
then
    FREQ=$(bc <<<"scale=2; $BASE_CLOCK / 1000")
    likwid-setFrequencies -reset
    likwid-setFrequencies -g performance
    likwid-setFrequencies -t 0
    likwid-setFrequencies -f "$FREQ"
    likwid-setFrequencies -p
fi

EOF
