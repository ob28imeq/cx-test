# source ${PYTHON_VENV}/bin/activate

mpirun --version

DIR='apps/benchmarks/UniformGridCPU'
ARGUMENT="simulation_setup/benchmark_configs.py"
CORES=$(nproc --all)
MPIRUN="likwid-mpirun -n ${CORES}"

sed -i 's/^\(validation_run\)/# \1/' ${BUILD_DIR}/${DIR}/$ARGUMENT
sed -i 's/^# \(single_node_benchmark\)/\1/' ${BUILD_DIR}/${DIR}/$ARGUMENT
BLOCK_SIZES=(8 16 32 64 128)

EXECUTABLES=$(ls ${BUILD_DIR}/${DIR}/UniformGridCPU_*)

for executable in ${EXECUTABLES} ;
do
    for bs in ${BLOCK_SIZES[@]};
    do
        pattern="s/block_sizes = \[.*\]/block_sizes = [($bs, $bs, $bs)]/"
        sed -i "$pattern" ${BUILD_DIR}/${DIR}/$ARGUMENT
        grep "block_sizes" ${BUILD_DIR}/${DIR}/$ARGUMENT
        basename_exe=$(basename ${executable})
        cmd="$MPIRUN $executable ${BUILD_DIR}/${DIR}/$ARGUMENT"
        if [ -n "$LIKWIDPRFCTR" ];
        then
            cmd="$LIKWIDPRFCTR -o likwid_${basename_exe}_${bs}_${PERF_GROUP}.json ${cmd}"
        fi
        echo "Runing $cmd"
        $cmd || echo "$cmd failed"
    done
done
