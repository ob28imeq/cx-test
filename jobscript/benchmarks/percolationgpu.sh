DIR='apps/benchmarks/Percolation'
PROFILING="ncu --set=full"
ARGUMENT="benchmark.prm"
CORES=$(nproc --all)
MPIRUN=""

export CUDA_VISIBLE_DEVICES=${VISABLE_DEVICE}

EXECUTABLE=Percolation
FULL_EXECUTABLE="${BUILD_DIR}/${DIR}/${EXECUTABLE}"
FULL_ARGUMENT="${BUILD_DIR}/${DIR}/${ARGUMENT}"

dev_name=$(nvidia-smi --id=${VISABLE_DEVICE} --query-gpu=gpu_name --format=csv,noheader 2> /dev/null | tr -d '[:blank:]' || echo "GPU_${VISABLE_DEVICE}")

PROFILE_EXT=".ncu-rep"
CSV_EXT=".csv"

# Function to run the command with profiling
run_with_profiling() {
    local profile_file=$1
    local db_file=$2
    local cmd=$3

    echo "Running $cmd with profiling"
    export DB_FILE=$db_file
    $cmd && ncu -i "${profile_file}${PROFILE_EXT}" --csv --page=raw > "${profile_file}${CSV_EXT}" || echo "$cmd failed"
}

# Function to run the command without profiling
run_without_profiling() {
    local db_file=$1
    local cmd=$2

    echo "Running $cmd without profiling"
    export DB_FILE=$db_file
    $cmd || echo "$cmd failed"
}

# Setup common command parameters
cmd_channelflow="${MPIRUN} ${FULL_EXECUTABLE} ${FULL_ARGUMENT} -NumericalSetup.useParticles=false"
cmd_percolation="${MPIRUN} ${FULL_EXECUTABLE} ${FULL_ARGUMENT}"

# First test case: ChannelFlow
profile_file_channelflow="ncuprofile_channelflow_${dev_name}"
db_file_channelflow_profile="gpu_profile_percolation_${dev_name}.sqlite3"
db_file_channelflow_benchmark="gpu_benchmark_percolation_${dev_name}.sqlite3"
cmd_channelflow_profile="${PROFILING} -o $profile_file_channelflow $cmd_channelflow -NumericalSetup.timeSteps=2"

run_with_profiling "$profile_file_channelflow" "$db_file_channelflow_profile" "$cmd_channelflow_profile"
run_without_profiling "$db_file_channelflow_benchmark" "$cmd_channelflow"

# Second test case: Percolation
profile_file_percolation="ncuprofile_percolation_${dev_name}"
db_file_percolation_profile="gpu_profile_percolation_${dev_name}.sqlite3"
db_file_percolation_benchmark="gpu_benchmark_percolation_${dev_name}.sqlite3"
cmd_percolation_profile="${PROFILING} -o $profile_file_percolation $cmd_percolation -NumericalSetup.timeSteps=2"

run_with_profiling "$profile_file_percolation" "$db_file_percolation_profile" "$cmd_percolation_profile"
run_without_profiling "$db_file_percolation_benchmark" "$cmd_percolation"
