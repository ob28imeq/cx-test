# source ${PYTHON_VENV}/bin/activate


DIR='apps/benchmarks/UniformGridGPU'
PROFILING="ncu --set=full"
ARGUMENT="simulation_setup/benchmark_configs.py"
CORES=$(nproc --all)
MPIRUN=""

export CUDA_VISIBLE_DEVICES=${VISABLE_DEVICE}

sed -i 's/^\(validation_run\)/# \1/' ${BUILD_DIR}/${DIR}/$ARGUMENT
sed -i 's/^# \(profiling\)/\1/' ${BUILD_DIR}/${DIR}/$ARGUMENT
EXECUTABLES=$(ls ${BUILD_DIR}/${DIR}/UniformGridGPU_*)

dev_name=$(nvidia-smi --id=${VISABLE_DEVICE} --query-gpu=gpu_name --format=csv,noheader 2> /dev/null | tr -d '[:blank:]' || echo "GPU_${VISABLE_DEVICE}")

for executable in ${EXECUTABLES} ;
do
    basename_exe=$(basename ${executable})
    profile_file="ncuprofile_${basename_exe}_${dev_name}"
    cmd="${PROFILING} -o $profile_file $MPIRUN $executable ${BUILD_DIR}/${DIR}/$ARGUMENT"
    export DB_FILE="gpu_profile_${dev_name}.sqlite3"
    echo "Runing $cmd"
    $cmd && ncu -i "${profile_file}.ncu-rep" --csv --page=raw > "$(basename -s ".ncu-rep" ${profile_file})".csv || echo "${cmd} failed"
done

sed -i 's/^\(profiling\)/# \1/' ${BUILD_DIR}/${DIR}/$ARGUMENT
sed -i 's/^# \(single_gpu_benchmark\)/\1/' ${BUILD_DIR}/${DIR}/$ARGUMENT
# BLOCK_SIZES=(32 64 128 256)
BLOCK_SIZES=(128)


for executable in ${EXECUTABLES} ;
do
    for bs in ${BLOCK_SIZES[@]};
    do
        pattern="s/block_sizes = \[.*\]/block_sizes = [($bs, $bs, $bs)]/"
        sed -i "$pattern" ${BUILD_DIR}/${DIR}/$ARGUMENT
        grep "block_sizes" ${BUILD_DIR}/${DIR}/$ARGUMENT
        basename_exe=$(basename ${executable})
        cmd="$MPIRUN $executable ${BUILD_DIR}/${DIR}/$ARGUMENT"
        echo "Runing $cmd"
        export DB_FILE="gpu_benchmark_${dev_name}.sqlite3"
        $cmd || echo "$cmd failed"
    done
done
