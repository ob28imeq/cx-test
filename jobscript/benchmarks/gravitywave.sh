# source ${PYTHON_VENV}/bin/activate

mpirun --version

DIR='apps/benchmarks/FreeSurfacePerformance'
CORES=$(nproc)
# PIN_EXPR="N:0-$((CORES - 1))"
MPIRUN="likwid-mpirun -n ${CORES}"

ARGUMENT="GravityWave.prm"
executable="${BUILD_DIR}/${DIR}/GravityWaveCodegenBenchmark"
basename_exe=$(basename ${executable})
block_decompositions=("2D" "3D")
barrier_after_sweep=("true" "false")

for blockdecomp in ${block_decompositions[@]} ;
do
    for barrier in ${barrier_after_sweep[@]};
    do
        cmd="$MPIRUN $executable ${BUILD_DIR}/${DIR}/$ARGUMENT -BenchmarkParameters.blockDecomposition=${blockdecomp} -BenchmarkParameters.barrier_after_sweep=${barrier}"
        if [ -n "$LIKWIDPRFCTR" ];
        then
            cmd="$LIKWIDPRFCTR -o likwid_${basename_exe}_${PERF_GROUP}.json ${cmd}"
        fi
        echo "Running $cmd"

        $cmd
    done
done
